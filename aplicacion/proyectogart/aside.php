<?php
?>

<aside id='elAside'>


    <?php

    if (isset($_SESSION['admin'])){
        session_destroy();
    }

    if (isset($_SESSION['usuario'])) {

        echo "<h2>Bienvenido ".$_SESSION["usuario"]->nombre." </h2>";
        echo "<img style='width: 240px;height: 240px;' id='elAvatar' alt='avatar' title='avatar identificativo'";
        if (($_SESSION['usuario']->avatar == "")){

            echo "src='img/avatar.png'>";
        } else {
            echo "src='".$_SESSION["usuario"]->avatar."'>";

        };
          echo "
    <p><ul>
        <li><a href='perfil.php'>Perfil</a></li>
        <li><a href='mensajes.php'>Mensajes (";
          echo $_SESSION['usuario']->correosNoLeidos();
    echo ")</a></li>
        <li><a href='favoritos.php'>Favoritos</a></li>
        <li><a href='logout.php'>Salir</a></li>
    </ul></p>";


    } else {

       echo " <div class='white-panel'>
                            <div class='login-show'>
                                <h2>LOGIN</h2>
                                <form name='' action='acciones.php' method='post'>
                                    <div class='form-group row'>
                                        <div class='col-8'>

                                            <input name='nick' placeholder='Usuario' class='form-control here' type='text'>
                                        </div>
                                    </div>
                                    <div class='form-group row'>
                                        <div class='col-8'>
                                            <input  name='pass' placeholder='Contraseña' class='form-control here' type='password'>
                                        </div>
                                    </div>

                                    <div class='form-group row'>
                                        <div class='col-8'>
                                            <input  name='botonEntrar' value='entrar' class='btn btn-primary' type='submit'>
                                        </div>
                                    </div>
                                    <div class='col-8'>
                                        <a  href='registro.php'> Registrarse</a>
                                    </div>
                                </form>

                            </div>

                        </div>";

    } ?>



</aside>
