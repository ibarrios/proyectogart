<?php

include 'header.php';

if (!isset($_SESSION['admin']->esAdmin)) {
    header('location: loginadmin.php');
}

?>

<!doctype html>
<html lang="en">
<head>
    <title>Administracion</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="dist/sweetalert.css">
    <link rel="stylesheet" href="css/css.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>



<body class="misEstilos">


<div class="laImagen" title="imagen princial de la web">

</div>
<div class="container">

    <div class="row py-3">
        <div class="col-3 order-2" id="sticky-sidebar">
            <div class="sticky-top">
                <div class="nav flex-column">

                </div>
            </div>
        </div>
        <div class="col" id="main">
            <article>
                <article>
                    <h2>Banear</h2>
                    <ol style="list-style: none">
                        <?php $_SESSION['admin'] -> listarUser(); ?>
                    </ol>
                </article>

                <article>
                    <h2>Dar permisos</h2>
                    <form name="" action="acciones.php" method="post">
                        <div class="form-group row">
                            <label for="name" class="col-4 col-form-label">Dar permisos de creación a los usuarios</label>
                            <div class="col-8">
                                <?php
                                echo "<form action='acciones.php' method='post'>";
                                   if (consultaPermisos()) {
                                   echo "<input type='checkbox'  name='ponerPermiso' onchange='this.form.submit()'  >";
                                   } else {
                                   echo "<input type='checkbox' name='quitarPermiso'  value='quitarPermiso' onchange='this.form.submit()' checked >";
                                   echo "<input type ='hidden' name ='quitarPermiso'>";
                                   };
                                   echo "</form>";?>
                            </div>
                        </div>

                    </form>
                </article>

            </article>
        </div>
    </div>
</div>

</body>
<?php require_once 'footer.php';?>

</html>

