<?php
require_once 'header.php';

if (isset($_SESSION['admin']->esAdmin)){}
else if (isset($_SESSION['usuario']->nick)) {
    if ($_SESSION['usuario']->consultaPermisos()) {
    } else {
        header('Location: index.php');
    }
} else {
    header('Location: index.php');

}

?>




<!doctype html>
<html lang="en">
<head>
    <title>Crear Item</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="dist/sweetalert.css">
    <link rel="stylesheet" href="css/css.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<?php if (isset($_SESSION['mensaje'])){
    alerta($_SESSION['mensaje']);
    unset($_SESSION['mensaje']);
}
?>

<body class="misEstilos">
<SCRIPT LANGUAGE="JavaScript">
    function popUp(URL) {
        day = new Date();
        id = day.getTime();
        eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=0,location=1,statusbar=1,menubar=0,resizable=0,width=500,height=500,left = 710,top = 290');");
    }
</script>
<script type="text/javascript">
    function enviar_formulario(){
        document.formulario1.submit()
    }
</script>
<div class="laImagen" title="imagen princial de la web">

</div>
<div class="container">

    <div class="row py-3">
        <div class="col-3 order-2" id="sticky-sidebar">
            <div class="sticky-top">
                <div class="nav flex-column">
                    <?php if (isset($_SESSION["admin"]->esAdmin)){}
                    else {require_once 'aside.php';}?>

                </div>
            </div>
        </div>
        <div class="col" id="main">
            <article>
                <article>
                    <h2>Crear entrada</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <form name="" action="acciones.php" method="post">

                                <div class="form-group row">
                                    <label for="name" class="col-4 col-form-label">Introduce el titulo</label>
                                    <div class="col-8">
                                        <input name="titulo" placeholder="Introduce Título" required class="form-control here" type="text">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="name" class="col-4 col-form-label">Introduce juego</label>
                                    <div class="col-8">
                                        <input name="juego" placeholder="Introduce juego" required class="form-control here" type="text">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="name" class="col-4 col-form-label">Introduce tu compañia</label>
                                    <div class="col-8">
                                        <input  name="compania" placeholder="Introduce la compañia " required class="form-control here" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-4 col-form-label">Introduce fecha</label>
                                    <div class="col-8">
                                        <input name="fecha"  placeholder="Introduce la fecha " required class="form-control here" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-4 col-form-label">Subir imagen o añadir enlace a Youtube</label>
                                    <div class="col-8">
                                        <input type="text" required name="enlace" id="img"/><a class="btn btn-primary" href="javascript:popUp('subir_imagen.php');">Subir imagen</a>                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-4 col-form-label">Tipo de contenido</label>
                                    <div class="col-8">
                                        <select name="categoria" class="form-select" aria-label="Default select example">
                                            <option value="video">Video</option>
                                            <option value="Audio">Audio</option>
                                            <option value="youtube">Youtube</option>
                                            <option value="Imagen">Imagen</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-4 col-8">
                                        <input name="botonItem" value ="Crear Item" type="submit" class="btn btn-primary">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </article>


                    <?php if(isset($_SESSION['admin']->esAdmin)){
                        echo "<article><h2>Editar Items</h2>";
                        $_SESSION['item']->listadoItems();
                        echo "</article>";
                    }?>


            </article>



        </div>
    </div>
</div>

</body>
<?php require_once 'footer.php';?>

</html>
