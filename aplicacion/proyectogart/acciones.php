<?php

include "Clases/funcionesUser.php";
include "Clases/User.php";
include "Clases/funcionesAdmin.php";
include "Clases/Cifrado.php";
include "Clases/Item.php";


session_start();



if (isset($_POST["botonRegistro"])) {
    $pdo=new PDO("mysql:host=localhost;dbname=proyectogar;charset=utf8", 'root', '');
    $nick = $_SESSION['cifrador']  -> cifrar(filter_var($_POST["nick"], FILTER_SANITIZE_STRING));

    $consulta = $pdo->prepare("SELECT * FROM admin where nick = '".$nick."'  ");
    $consulta->execute();
    $existeUser = $consulta -> rowCount();

    if (!$existeUser == 1){
        registrarUser();
        $_SESSION['mensaje'] = "Usuario Creado";

    } else {
        $_SESSION['mensaje'] = "El usuario ya existe";
        header('Location:' . getenv('HTTP_REFERER'));
    }

}

if (isset($_POST["botonItem"])) {

    $_SESSION['item']->crearItem();

}

if (isset($_POST["botonEntrar"])) {

    $nick = filter_var($_POST["nick"], FILTER_SANITIZE_STRING);
    $pass = $_POST["pass"];
    if(existeUser(($nick),($pass))){

        usuariosConDatos(($nick));
        header('Location: perfil.php');

    }  else {
        $_SESSION['mensaje'] = "El usuario y/o la contraseña son incorrectos";
        header('Location:' . getenv('HTTP_REFERER'));

    }

}

if (isset($_POST["botonBorrarUser"])) {

    if (existeUser(($_SESSION['usuario'] -> nick), (filter_var($_POST["passBorrar"], FILTER_SANITIZE_STRING)))) {

        $_SESSION['usuario']->bajaUser();
    } else {
        $_SESSION['mensaje'] = "Contraseña incorrecta";

        header('Location:' . getenv('HTTP_REFERER'));
    }

}


if (isset($_POST['botonCambiarDatos'])) {


    if( $_SESSION['usuario']->modificarDatos($_POST["nombre"],$_POST["apellidos"],$_POST["telefono"],$_POST["email"])){

        usuariosConDatos($_SESSION['usuario'] -> nick);
        header('Location: perfil.php');

    } else {
        $_SESSION['mensaje'] = "Contraseña incorrecta";

    }

}

if (isset($_POST['botonEntrarAdmin'])) {

    if (existeAdmin($_POST['userAdmin'],$_POST['passAdmin'])){
        header('Location: administracion.php');

    } else {
        $_SESSION['mensaje'] = "Usuario y/o contraseña incorrecta";

        header('Location:' . getenv('HTTP_REFERER'));

    }

}

if (isset($_POST['botonCambiarPass'])) {

    if (existeUser(($_SESSION['usuario'] -> nick),$_POST['passOld'])) {
        if ($_POST['passNew1'] == $_POST['passNew2']) {
            $_SESSION['usuario'] -> cambiarPass($_POST['passNew1']);
            header('Location: perfil.php');
        } else {
            $_SESSION['mensaje'] = "Contraseña incorrecta";
            header('Location: perfil.php');
        }
    } else {
        $_SESSION['mensaje'] = "Contraseña incorrecta";
        header('Location: perfil.php');
    }
}

if (isset($_POST['elAvatar'])) {
    $errores = "";
    $formatos = array('gif', 'png', 'jpg');
    $nombre_img = $_FILES['imagen']['name'];
    $ext = pathinfo($nombre_img, PATHINFO_EXTENSION);


    if (!in_array($ext, $formatos)) {
        header('Location:' . getenv('HTTP_REFERER'));
        $_SESSION['mensaje'] = "Fichero no soportado";
        header('Location: perfil.php');    }

    if($_FILES['imagen']['size'] >= 2097152){ // 2MB
        header('Location:' . getenv('HTTP_REFERER'));
        $_SESSION['mensaje'] = "La imagen debe ser de la extensión PNG GIF o JPG";
        header('Location: perfil.php');

    }

    if(empty($errores)){
        $path = "img/". basename($_FILES['imagen']['name']);

        if(move_uploaded_file($_FILES['imagen']['tmp_name'], $path)) {
            echo "El archivo ".  basename( $_FILES['imagen']['name']). " ha sido subido";
            $_SESSION['usuario'] ->subirAvatar($path);
            usuariosConDatos($_SESSION['usuario'] -> nick);
            header('Location: perfil.php');

        } else{
            $_SESSION['mensaje'] = "La imagen debe ser de la extensión PNG GIF o JPG";
            header('Location: perfil.php');        }
    }else{
        echo $errores;
    }
}


if (isset($_POST['botonBorrarUserAdmin'])) {
    $_SESSION['admin']->borrarUser($_POST['elUser']);
}


if (isset($_POST['ponerPermiso']) || isset($_POST['quitarPermiso'])) {
    if (isset($_POST['ponerPermiso'])){
        $_SESSION['admin']->permisoCrearItem(1);
       header('Location: administracion.php');

    } else if (isset($_POST['quitarPermiso'])) {
        $_SESSION['admin']->permisoCrearItem(0);
        header('Location: administracion.php');
    }
}

if (isset($_POST['botonBuzonPerfil'])) {
    if ($_POST['cuerpoMensaje'] !="") {
        $_SESSION['usuario'] ->enviarMensaje($_POST['cuerpoMensaje'],$_POST['destinoMensaje']);
    } else {
        $_SESSION['mensaje'] = "Rellena el mensaje";

        header('Location:' . getenv('HTTP_REFERER'));
    }
}

if (isset($_POST['botonMarcarLeido'])) {
    $_SESSION['usuario'] ->modificarLeido(1,$_POST['idDelMensaje']);
}

if (isset($_POST['botonMarcarNoLeido'])) {
    $_SESSION['usuario'] ->modificarLeido(0,$_POST['idDelMensaje']);

}

if (isset($_POST['botonBorrarMensaje'])) {
    $_SESSION['usuario'] ->borrarMensaje($_POST['idDelMensaje']);

}

if (isset($_POST['botonResponder'])) {
    if ($_POST['laRespuesta'] !="") {
        $_SESSION['usuario'] ->enviarMensaje($_POST['laRespuesta'],$_POST['elUser']);


    } else {
        $_SESSION['mensaje'] = "Escribe una respuesta";

        header('Location:' . getenv('HTTP_REFERER'));    }
}

if (isset($_POST['borrarItem'])){
    $_SESSION['item']->borrarItem($_POST['elId']);
    $_SESSION['mensaje'] = "Entrada borrada";
}

if (isset($_POST['idDelItem'])) {
    $pdo=new PDO("mysql:host=localhost;dbname=proyectogar;charset=utf8", 'root', '');

        if (isset($_POST['itemFavorito'])){
            $stmt = ("INSERT INTO likes (id_item,id_usuario) 
            VALUES ('".$_POST['idDelItem']."','".$_SESSION['usuario']->id."')");
            $pdo->exec($stmt);
            header('Location:index.php');
        } else {

            $stmt = $pdo->prepare("delete from likes where id_item = '".$_POST['idDelItem']."' && id_usuario = '".$_SESSION['usuario']->id."'");
            $stmt->execute();
            header('Location:index.php');
        }

}

if (isset($_POST['editarItem'])){

    $_SESSION['item']->editarItem($_POST['elPiso'],$_POST['elJuego'],$_POST['laCompania'],
        $_POST['laFecha'],$_POST['categoria'],$_POST['elEnlace'],$_POST['elId']);
}



if (isset($_POST['elContenido'])){

    if ($_POST['laCategoria'] =="youtube"){

        $path = substr(($_POST['imagen']),-11);
        $_SESSION['item']->modificarContenido($_POST['elId'],$path);
    } else {
        var_dump("derp");
        $errores = "";
        $formatos = array('mp4', 'mp3', 'jpeg' , 'png' , 'gif', 'jpg');
        $nombre_img = $_FILES['imagen']['name'];
        $ext = pathinfo($nombre_img, PATHINFO_EXTENSION);


        if (!in_array($ext, $formatos)) {
            header('Location:' . getenv('HTTP_REFERER'));
            $_SESSION['mensaje'] = "Fichero no soportado";
        }

        if($_FILES['imagen']['size'] >= 1002097152){ // 2MB
            header('Location:' . getenv('HTTP_REFERER'));
            $_SESSION['mensaje'] = "El contenido debe ser de la extensión PNG GIF JPG JPEG MP3 MP4  ";


        }

        if(empty($errores)){
            $path = "img/". basename($_FILES['imagen']['name']);

            if(move_uploaded_file($_FILES['imagen']['tmp_name'], $path)) {
                echo "El archivo ".  basename( $_FILES['imagen']['name']). " ha sido subido";
                $_SESSION['item']->modificarContenido($_POST['elId'],$path);
            }

        } else{
            $_SESSION['mensaje'] = "El contenido debe ser de la extensión PNG GIF JPG JPEG MP3 MP4  ";
        }    }


}

?>