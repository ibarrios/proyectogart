<?php

require_once 'header.php';


if (!isset($_SESSION['cifrador']) ){
    header('Location: index.php');}
?>



<!doctype html>
<html lang="en">
<head>
    <title>Buzón</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="dist/sweetalert.css">
    <link rel="stylesheet" href="css/css.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<?php if (isset($_SESSION['mensaje'])){
    alerta($_SESSION['mensaje']);
    unset($_SESSION['mensaje']);
}
?>
<body class="misEstilos">


<div class="laImagen" title="imagen princial de la web">

</div>
<div class="container">

    <div class="row py-3">
        <div class="col-3 order-2" id="sticky-sidebar">
            <div class="sticky-top">
                <div class="nav flex-column">
                    <?php require_once 'aside.php';?>
                </div>
            </div>
        </div>
        <div class="col" id="main">
            <article>
                <article>
                    <h2>Enviar mensaje</h2>
                    <form name="" action="acciones.php" method="post">
                        <div class="form-group row">
                            <label for="username" class="col-4 col-form-label">Destino</label>
                            <div class="col-8">
                                <input name="destinoMensaje" value="" placeholder="" class="form-control here"  type="text">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="username" class="col-4 col-form-label">Cuerpo</label>
                            <div class="col-8">
                                <textarea id="username" rows="6" cols="40" name="cuerpoMensaje" placeholder="introduce aqui tu mensaje" class="form-control here"  ></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="offset-4 col-8">
                                <input name="botonBuzonPerfil" value ="Enviar mensaje" type="submit" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </article>
                <article>
                    <h2>No leidos</h2>
                        <ol style="list-style: none">
                            <?php $_SESSION['usuario']->listadoCorreos(0,$_SESSION['usuario']->nick);?>
                        </ol>
                </article>
                <article>
                    <h2>Leidos</h2>

                        <ol style="list-style: none">
                            <?php $_SESSION['usuario']->listadoCorreos(1,$_SESSION['usuario']->nick);?>
                        </ol>

                </article>
            </article>
        </div>
    </div>
</div>

</body>
<?php require_once 'footer.php';?>

</html>
