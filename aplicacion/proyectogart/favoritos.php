<?php
require_once 'header.php';


if (!isset($_SESSION['usuario']->nick)){
    header('Location: index.php');}
?>



<!doctype html>
<html lang="en">
<head>
    <title>Favoritos</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="dist/sweetalert.css">
    <link rel="stylesheet" href="css/css.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<?php if (isset($_SESSION['mensaje'])){
    alerta($_SESSION['mensaje']);
    unset($_SESSION['mensaje']);
}
?>

<body class="misEstilos">


<div class="laImagen" title="imagen princial de la web">

</div>
<div class="container">

    <div class="row py-3">
        <div class="col-3 order-2" id="sticky-sidebar">
            <div class="sticky-top">
                <div class="nav flex-column">
                    <?php require_once 'aside.php'?>

                </div>
            </div>
        </div>
        <div class="col" id="main">
            <article>

                    <?php $_SESSION['item']->buscarTitulo("x","x",$_SESSION['usuario']->nick,2)?>
            </article>
        </div>
    </div>
</div>

</body>
<?php require_once 'footer.php';?>

</html>
