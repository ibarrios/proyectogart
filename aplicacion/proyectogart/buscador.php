<?php
require_once 'header.php';

if (!isset($_POST)){
    header('Location:index.php');

} else if(isset($_POST['palabra'])) {
    $tabla="titulo";
    $busqueda = $_POST['palabra'];

} else if (isset($_POST['juego'])) {
    $tabla="juego";
    $busqueda = $_POST['juego'];

} else if (isset($_POST['compania'])) {
    $tabla="compania";
    $busqueda = $_POST['compania'];

} else if (isset($_POST['categoria'])) {
    $tabla="categoria";
    $busqueda = $_POST['categoria'];

} else if (isset($_POST['ano'])) {
    $tabla="fecha";
    $busqueda = $_POST['ano'];

} else if (isset($_POST['autor'])) {
    $tabla = "autor_entrada";
    $busqueda = $_POST['autor'];

}
    ?>



<!doctype html>
<html lang="en">
<head>
    <title>Buscador</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="dist/sweetalert.css">
    <link rel="stylesheet" href="css/css.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<?php if (isset($_SESSION['mensaje'])){
    alerta($_SESSION['mensaje']);
    unset($_SESSION['mensaje']);
}
?>

<body class="misEstilos">

<div class="laImagen" title="imagen princial de la web">

</div>
<div class="container">

    <div class="row py-3">
        <div class="col-3 order-2" id="sticky-sidebar">
            <div class="sticky-top">
                <div class="nav flex-column">
                    <?php  if (isset($_SESSION["admin"]->esAdmin)){}
                    else {require_once 'aside.php';}?>

                </div>
            </div>
        </div>
        <div class="col" id="main">
            <article>
                <?php $_SESSION['item']-> buscarTitulo($tabla,$busqueda,"x",0);?>


            </article>
        </div>
    </div>
</div>

</body>
<?php require_once 'footer.php';?>

</html>
