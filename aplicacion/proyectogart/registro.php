<?php
require_once 'header.php';

?>



<!doctype html>
<html lang="en">
<head>
    <title>Registro</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="dist/sweetalert.css">
    <link rel="stylesheet" href="css/css.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<?php if (isset($_SESSION['mensaje'])){
    alerta($_SESSION['mensaje']);
    unset($_SESSION['mensaje']);
}
?>

<body class="misEstilos">


<div class="laImagen" title="imagen princial de la web">

</div>
<div class="container">

    <div class="row py-3">
        <div class="col-3 order-2" id="sticky-sidebar">
            <div class="sticky-top">
                <div class="nav flex-column">
        <?php require_once 'aside.php'?>
                </div>
            </div>
        </div>
        <div class="col" id="main">
            <article>

                <div class="row">
                    <div class="col-md-12">
                        <form name="" action="acciones.php" method="post">

                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Introduce tu usuario</label>
                                <div class="col-8">
                                    <input name="nick" placeholder="Introduce Usuario" required class="form-control here" type="text">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Introduce tu contraseña</label>
                                <div class="col-8">
                                    <input name="pass" placeholder="Introduce contraseña" required class="form-control here" type="password">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Introduce tu Nombre</label>
                                <div class="col-8">
                                    <input  name="nombre" placeholder="Introduce tu nombre" required class="form-control here" type="text">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Introduce tus apellido</label>
                                <div class="col-8">
                                    <input name="apellidos"  placeholder="Introduce tus apellidos " required class="form-control here" type="text">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Introduce tu correo electrónico</label>
                                <div class="col-8">
                                    <input  name="email"  placeholder="Introduce email" required class="form-control here" type="email">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Introduce tu teléfono</label>
                                <div class="col-8">
                                    <input name="telefono"  placeholder="Introduce Teléfono"  required class="form-control here" type="tel">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="offset-4 col-8">
                                    <input name="botonRegistro" value ="Registrarse" type="submit" class="btn btn-primary">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
        </div>
    </div>
            </article>
        </div>
    </div>
</div>

</body>
<?php require_once 'footer.php';?>

</html>
