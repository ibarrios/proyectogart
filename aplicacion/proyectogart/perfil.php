<?php


require_once 'header.php';


if (!isset($_SESSION['usuario']->nick)){
    header('Location: index.php');}
?>



<!doctype html>
<html lang="en">
<head>
    <title>Perfil</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="dist/sweetalert.css">
    <link rel="stylesheet" href="css/css.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<?php if (isset($_SESSION['mensaje'])){
    alerta($_SESSION['mensaje']);
    unset($_SESSION['mensaje']);
}
?>

<body class="misEstilos">


<div class="laImagen" title="imagen princial de la web">

</div>
<div class="container">

    <div class="row py-3">
        <div class="col-3 order-2" id="sticky-sidebar">
            <div class="sticky-top">
                <div class="nav flex-column">
                    <?php require_once 'aside.php'?>

                </div>
            </div>
        </div>
        <div class="col" id="main">
            <article>
                <article>
                    <h2>Perfil</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <form name="" action="acciones.php" method="post">
                                <div class="form-group row">
                                    <label for="username" class="col-4 col-form-label">Nick</label>
                                    <div class="col-8">
                                        <input readonly name="nick" value="<?php echo $_SESSION['usuario'] -> nick;?>" placeholder="" class="form-control here"  type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="username" class="col-4 col-form-label">Nombre</label>
                                    <div class="col-8">
                                        <input  name="nombre" value="<?php echo $_SESSION['usuario'] -> nombre;?>" placeholder="Introduce el nombre" class="form-control here"  type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-4 col-form-label">Apellidos</label>
                                    <div class="col-8">
                                        <input name="apellidos" value="<?php echo $_SESSION['usuario'] -> apellidos;?>" placeholder="Introduce los apellidos" class="form-control here" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-4 col-form-label">Email</label>
                                    <div class="col-8">
                                        <input  name="email" placeholder="Introduce tu correo"value="<?php echo $_SESSION['usuario'] -> email;?>" class="form-control here"  type="email">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="website" class="col-4 col-form-label">Telefono</label>
                                    <div class="col-8">
                                        <input  name="telefono" placeholder="Introduce tu Teléfono" style="-moz-appearance: textfield;" value="<?php echo $_SESSION['usuario'] -> telefono;?>" class="form-control here" type="tel">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-4 col-8">
                                        <input name="botonCambiarDatos" value ="Modificar" type="submit" class="btn btn-primary">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </article>

                <article>
                    <h2>Avatar</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label">Cambia tu avatar<br> tamaño 240x240</label>
                                <div class="col-8">

                                    <form action="acciones.php" enctype="multipart/form-data" method="post">
                                        <input id="imagen" name="imagen" size="30" type="file" />
                                        <input type="submit" class='btn btn-primary'name="elAvatar" value="Subir Imagen" />
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </article>
                <article>
                    <h2>Dar de baja</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <form name="" action="acciones.php" method="post">
                                <div class="form-group row">
                                    <label for="name" class="col-4 col-form-label">Introduce tu contraseña para borrar tu cuenta</label>
                                    <div class="col-8">

                                        <input name="passBorrar" placeholder="Introduce contraseña" class="form-control here" type="password">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-4 col-8">
                                        <input style="background-color: red" name="botonBorrarUser" value ="Borrar" type="submit" class="btn btn-primary">
                                    </div>
                                </div>
                            </form>
                        </div>
                </article>
                <article>
                    <h2>Cambiar Contraseña</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <form name="" action="acciones.php" method="post">
                                <div  class="form-group row">
                                    <label for="username" class="col-4 col-form-label">Introduce tu vieja contraseña</label>
                                    <div class="col-8">
                                        <input id="username" id="passOld" name="passOld" placeholder="Introduce el nombre" class="form-control here"  type="password">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-4 col-form-label">Introduce tu nueva contraseña</label>
                                    <div class="col-8">
                                        <input id="name" id="passNew1" name="passNew1" placeholder="Introduce nueva contraseña" class="form-control here" type="password">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="lastname" class="col-4 col-form-label">Introduce otra vez tu nueva contraseña</label>
                                    <div class="col-8">
                                        <input  id="passNew2" name="passNew2" placeholder="Introduce de nuevo" class="form-control here" type="password">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="offset-4 col-8">
                                        <input name="botonCambiarPass" value ="Cambiar" type="submit" class="btn btn-primary">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </article>
            </article>
        </div>
    </div>
</div>

</body>
<?php require_once 'footer.php';?>

</html>
