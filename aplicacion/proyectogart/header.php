<?php
include "Clases/Cifrado.php";
include 'Clases/funcionesAdmin.php';
include "Clases/funcionesUser.php";
include "Clases/User.php";
include "Clases/Item.php";
session_start();
$_SESSION['item'] = new Item();
$_SESSION['cifrador'] = new Cifrado();

?>

<header>
    <script type="text/javascript">
        function enviar_formulario(){
            document.formulario1.submit()
        }
    </script>
    <h1>Galeria de Arte</h1>

    <nav id ="vermenuB" class="navbar navbar-expand-lg navbar-light ">

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse " id="navbarTogglerDemo01">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Inicio</a>
                </li>
                <?php
                if (isset($_SESSION['usuario']->nick)){
                    echo "<li class='nav-item active'>
                    <a class='nav-link' href='favoritos.php'>Favoritos</a>
                </li>
                <li class='nav-item active'>
                    <a class='nav-link' href='perfil.php'>Perfil</a>
                </li>
                <li class='nav-item active'>
                    <a class='nav-link' href='mensajes.php'>Mensajes</a>
                </li>";
                    if ((consultaPermisos())){

                    } else {
                        echo"  <li class='nav-item active'>
                    <a class='nav-link' href='crearitem.php'>Crear Entrada</a>
                </li>";
                    };

                    echo "<li class='nav-item active'>
                    <a class='nav-link' href='logout.php'>Salir</a>
                </li>
                
                <form method='POST' action='buscador.php' method=post name='formulario1'>
 
                <li><input type='text' placeholder='Buscar' name='palabra' onkeypress=if (event.keyCode == 13) enviar_formulario()>
 
                </form>
                
                ";
                } else if (!isset($_SESSION['admin']->esAdmin)) {
                    echo" <li class='nav-item active'>
                    <a class='nav-link' href='login.php'>Login</a>
                </li>";
                } else if(isset($_SESSION['admin']->esAdmin)) {
                    echo "<li class='nav-item active'>
                    <a class='nav-link' href='administracion.php'>Panel de Administracion</a>
                </li>
                <li class='nav-item active'>
                    <a class='nav-link' href='crearitem.php'>Entradas</a>
                </li>
                <li class='nav-item active'>
                    <a class='nav-link' href='logout.php'>Salir</a>
                </li>    
                    ";
                }
                ?>
            </ul>
        </div>
    </nav>

    <nav id ="vermenuA" class="navbar navbar-expand-lg navbar-light">
        <div class="container-fluid">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php">Inicio</a>
                    </li>
                    <?php
                    if (isset($_SESSION['usuario']->nick)){
                        echo "<li class='nav-item active'>
                    <a class='nav-link' href='favoritos.php'>Favoritos</a>
                </li>
                <li class='nav-item active'>
                    <a class='nav-link' href='perfil.php'>Perfil</a>
                </li>
                <li class='nav-item active'>
                    <a class='nav-link' href='mensajes.php'>Mensajes</a>
                </li>";
                        if ((consultaPermisos())){

                        } else {
                            echo"  <li class='nav-item active'>
                    <a class='nav-link' href='crearitem.php'>Crear Entrada</a>
                </li>";
                        };

                        echo "<li class='nav-item active'>
                    <a class='nav-link' href='logout.php'>Salir</a>
                </li>
                
              <form method='POST' action='buscador.php' method=post name='formulario1'>
 
                <li><input type='text' placeholder='Buscar' name='palabra' onkeypress=if (event.keyCode == 13) enviar_formulario()>
 
                </form>
                
                ";
                    } else if (!isset($_SESSION['admin']->esAdmin)) {
                        echo" <li class='nav-item active'>
                    <a class='nav-link' href='login.php'>Login</a>
                </li>";
                    } else if(isset($_SESSION['admin']->esAdmin)) {
                        echo "<li class='nav-item active'>
                    <a class='nav-link' href='administracion.php'>Panel de Administracion</a>
                </li>
 
                <li class='nav-item active'>
                    <a class='nav-link' href='crearitem.php'>Entradas</a>
                </li>
                <li class='nav-item active'>
                    <a class='nav-link' href='logout.php'>Salir</a>
                </li>    
                    ";
                    }
                    ?>
                </ul>
            </div>
        </div>
    </nav>

</header>
