<?php

$pdo=new PDO("mysql:host=localhost;dbname=proyectogar;charset=utf8", 'root', '');

function registrarUser(){
    global $pdo;
    $pass = (filter_var($_POST["pass"], FILTER_SANITIZE_STRING));
    $pass = password_hash($pass,PASSWORD_DEFAULT);


        $nick = $_SESSION['cifrador']  -> cifrar(filter_var($_POST["nick"], FILTER_SANITIZE_STRING));
        $nombre = $_SESSION['cifrador']  -> cifrar(filter_var($_POST["nombre"], FILTER_SANITIZE_STRING));
        $apellidos = $_SESSION['cifrador']  ->cifrar(filter_var($_POST["apellidos"], FILTER_SANITIZE_STRING));
        $email = $_SESSION['cifrador']  ->cifrar(filter_var($_POST["email"], FILTER_SANITIZE_EMAIL));
        $telefono = $_SESSION['cifrador']  ->cifrar(filter_var($_POST["telefono"], FILTER_SANITIZE_STRING));

        $id_admin= "1";
        $sql = "INSERT INTO usuario (nick, pass, nombre, apellidos,email,telefono,id_admin) VALUES (
            :nick, 
            :pass, 
            :nombre,                                                             
            :apellidos, 
            :email,
            :telefono,
            :id_admin)";
        $stmt =$pdo ->prepare($sql);

        $stmt->bindParam(':nick', $nick, PDO::PARAM_STR);
        $stmt->bindParam(':pass', $pass, PDO::PARAM_STR);
        $stmt->bindParam(':nombre', $nombre, PDO::PARAM_STR);
        $stmt->bindParam(':apellidos', $apellidos, PDO::PARAM_STR);
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->bindParam(':telefono', $telefono, PDO::PARAM_STR);
        $stmt->bindParam(':id_admin', $id_admin, PDO::PARAM_STR);
        $stmt->execute();
        header('Location:' . getenv('HTTP_REFERER'));

    }


    function existeUser($nick,$pass) {
        global $pdo;


        $nick = $_SESSION['cifrador']  -> cifrar($nick);
        $consulta = $pdo ->prepare("SELECT * FROM usuario where nick = '".$nick."' ");
        $consulta->execute();
        $existePass = $consulta -> fetch(PDO::FETCH_ASSOC);

        if ((password_verify($pass,$existePass["pass"]))) {

            return true;
        } else {

            return false;
        }

    }

    function usuariosConDatos($nick){
        global $pdo;
        $nick = $_SESSION['cifrador']  -> cifrar($nick);
        $consulta = $pdo->prepare("SELECT * FROM usuario WHERE nick='".$nick."'");
        $consulta->execute();

        $datos = $consulta -> fetch(PDO::FETCH_ASSOC);
        $_SESSION['usuario'] = new User($datos['id'],$_SESSION['cifrador'] ->descifrar($datos['nick']),$datos['pass'],
            $_SESSION['cifrador'] ->descifrar($datos['nombre']),$_SESSION['cifrador'] ->descifrar($datos['apellidos']),
            $_SESSION['cifrador'] ->descifrar($datos['email']),$_SESSION['cifrador'] ->descifrar($datos['telefono'])
        , $datos['avatar']);


    }



    function alerta($message) {

        // Display the alert box
    echo "<script>swal('$message');</script>";
    }




