<?php


class User {
    public $id;
    public $nick;
    public $pass;
    public $nombre;
    public $apellidos;
    public $email;
    public $telefono;
    public $avatar;

    /**
     * User constructor.
     * @param $nick
     * @param $pass
     * @param $nombre
     * @param $apellidos
     * @param $email
     * @param $telefono
     */
    public function __construct($id,$nick, $pass, $nombre, $apellidos, $email, $telefono,$avatar)
    {
        $this ->id = $id;
        $this->nick = $nick;
        $this->pass = $pass;
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->email = $email;
        $this->telefono = $telefono;
        $this->avatar = $avatar;
    }


    /**
     * @return mixed
     */
    public function getNick()
    {
        return $this->nick;
    }

    /**
     * @param mixed $nick
     */
    public function setNick($nick)
    {
        $this->nick = $nick;
    }

    /**
     * @return mixed
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @param mixed $pass
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * @param mixed $apellidos
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @param mixed $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    /**
     * @return mixed
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param mixed $avatar
     */
    public function setAvatar($avatar): void
    {
        $this->avatar = $avatar;
    }




    function bajaUser(){
        $pdo=new PDO("mysql:host=localhost;dbname=proyectogar;charset=utf8", 'root', '');

        $statement = $pdo ->prepare("DELETE FROM usuario where nick = '" . $_SESSION['cifrador']->cifrar($_SESSION['usuario']->nick) . "'");
        $statement->execute();
        $_SESSION['mensaje'] = "Usuario dado de baja";
        session_destroy();
        header('Location: index.php');


    }

    function modificarDatos($nombre,$apellidos,$telefono,$email){
        $pdo=new PDO("mysql:host=localhost;dbname=proyectogar;charset=utf8", 'root', '');
        $nombre = $_SESSION['cifrador']  -> cifrar(filter_var($nombre, FILTER_SANITIZE_STRING));
        $apellidos = $_SESSION['cifrador']  ->cifrar(filter_var($apellidos, FILTER_SANITIZE_STRING));
        $telefono = $_SESSION['cifrador']  ->cifrar(filter_var($telefono, FILTER_SANITIZE_EMAIL));
        $email = $_SESSION['cifrador']  ->cifrar(filter_var($email, FILTER_SANITIZE_STRING));
        $sql = "UPDATE usuario SET nombre=?, apellidos=?, telefono=?, email=? where nick = '" . $_SESSION['cifrador']->cifrar($_SESSION['usuario']->nick) . "'";
        $pdo->prepare($sql)->execute([$nombre, $apellidos, $telefono,$email ]);
        return true;
        $_SESSION['mensaje'] = "Datos modificados";
    }

    function cambiarPass($pass){
        $pass = (filter_var($pass, FILTER_SANITIZE_STRING));
        $pass = password_hash($pass,PASSWORD_DEFAULT);

        $pdo=new PDO("mysql:host=localhost;dbname=proyectogar;charset=utf8", 'root', '');
        $sql = "UPDATE usuario SET pass=? where nick = '" . $_SESSION['cifrador']->cifrar($_SESSION['usuario']->nick) . "'";

        $pdo->prepare($sql)->execute([$pass]);
        $_SESSION['mensaje'] = "Contraseña cambiada";
        return true;
    }

    function subirAvatar($ruta){
        $pdo=new PDO("mysql:host=localhost;dbname=proyectogar;charset=utf8", 'root', '');
        $sql = "UPDATE usuario SET avatar=? where nick = '" . $_SESSION['cifrador']->cifrar($_SESSION['usuario']->nick) . "'";
        $pdo->prepare($sql)->execute([$ruta]);
        $_SESSION['mensaje'] = "Avatar cambiado";
    }


    function consultaPermisos(){
        $pdo=new PDO("mysql:host=localhost;dbname=proyectogar;charset=utf8", 'root', '');

        $consulta = $pdo->prepare("SELECT userCrea FROM admin");
        $consulta->execute();
        $resultado = $consulta -> fetch(PDO::FETCH_ASSOC);
        if ($resultado['userCrea'] == "1"){
            return true;
        } else {
            return false;
        }
    }


    function enviarMensaje($contenido,$recibe){

        $pdo=new PDO("mysql:host=localhost;dbname=proyectogar;charset=utf8", 'root', '');

        $consulta = $pdo->prepare("SELECT * FROM usuario where nick = '".$_SESSION['cifrador']->cifrar($recibe)."'");


        $consulta->execute();
        $existeUser = $consulta -> rowCount();

        if ($existeUser == 1) {
            $elDestino = $consulta -> fetch(PDO::FETCH_ASSOC);


            $id_usuario_envia = intval($_SESSION['usuario'] -> id);
            $id_usuario_recibe = intval($elDestino['id']);
            $contenido = $_SESSION['cifrador']  -> cifrar(filter_var($contenido, FILTER_SANITIZE_STRING));
            $recibe = $_SESSION['cifrador']  -> cifrar(filter_var($recibe, FILTER_SANITIZE_STRING));
            $envia = $_SESSION['cifrador']  -> cifrar($_SESSION['usuario']->nick);
            $leido = 0;


            $sql = "INSERT INTO buzon (envia, recibe, leido,contenido,id_usuario_envia,id_usuario_recibe)
                 VALUES ('$envia', '$recibe','$leido', '$contenido','$id_usuario_envia','$id_usuario_recibe')";

            $pdo->exec($sql);
            $_SESSION['mensaje'] = "Mensaje enviado";
            header('Location: mensajes.php');


        } else {
            $_SESSION['mensaje'] = "El usuario no existe";
            header('Location:' . getenv('HTTP_REFERER'));
            return false;
        }
    }

    function listadoCorreos($leido,$user) {
        $pdo=new PDO("mysql:host=localhost;dbname=proyectogar;charset=utf8", 'root', '');
        $resultado = $pdo->query("SELECT * FROM buzon where recibe = '".$_SESSION['cifrador']->cifrar($_SESSION['usuario']->nick)."'");
        $datos = $resultado -> fetchAll();

        foreach ($datos as $daticos) {
            $daticos['envia']=$_SESSION['cifrador']->descifrar($daticos['envia']);
            $daticos['recibe']=$_SESSION['cifrador']->descifrar($daticos['recibe']);
            $daticos['contenido']=$_SESSION['cifrador']->descifrar($daticos['contenido']);
            $daticos['leido'] = intval($daticos['leido']);


            if (($daticos['leido'])==$leido){
                if($daticos['recibe'] == $user){echo "<form action='acciones.php' method='post'>";
                    echo "Envia : <li style='padding-top: 10px'><input readonly type='text' name='elUser' 
                    id='elUser' value='".$daticos['envia']."'></li><br>";
                    echo "<li style='padding-top: 10px'><textarea readonly name='elMensaje' 
                    id='elMensaje'>".$daticos['contenido']."</textarea></li><br>";
                    if ($daticos['leido'] == 0) {echo "<input type='submit' class='btn btn-primary' name='botonMarcarLeido' value='Leido'>";
                    } else { echo "<input type='submit' class='btn btn-primary' name='botonMarcarNoLeido' value='Marcar como No leido'>";}}
                    echo "<input type='submit' name='botonBorrarMensaje' class='btn btn-primary' value='Borrar'>";
                    echo "<input type='submit' class='btn btn-primary' name='botonResponder' value='Responder'>";
                    echo "<li style='padding-top: 10px'><textarea name='laRespuesta' id='elMensaje' 
                        placeholder='Escribe aquí la respuesta para ".$daticos['envia']."'></textarea></li><br>";
                    echo "<input type='hidden' name='idDelMensaje' value='".$daticos['id']."'>";
                    echo "</form>";}
        }
    }

    function modificarLeido($leido,$id){

        $pdo=new PDO("mysql:host=localhost;dbname=proyectogar;charset=utf8", 'root', '');
        $stmt = $pdo->prepare("update buzon set leido = :leido where id = :id");

        $stmt->bindParam(':leido', $leido, PDO::PARAM_INT);
        $stmt->bindParam(':id', $id, PDO::PARAM_STR);
        $stmt->execute();

        header('Location:' . getenv('HTTP_REFERER'));
    }

    function borrarMensaje($id){
        $pdo=new PDO("mysql:host=localhost;dbname=proyectogar;charset=utf8", 'root', '');
        $stmt = $pdo->prepare("delete from buzon where id = :id");
        $stmt->bindParam(':id', $id, PDO::PARAM_STR);
        $stmt->execute();
        $_SESSION['mensaje'] = "Mensaje borrado";
        header('Location:' . getenv('HTTP_REFERER'));
    }

    function correosNoLeidos(){
        $pdo=new PDO("mysql:host=localhost;dbname=proyectogar;charset=utf8", 'root', '');
        $resultado = $pdo->query("SELECT * FROM buzon where recibe = '".$_SESSION['cifrador']->cifrar($_SESSION['usuario']->nick)."'");
        $datos = $resultado -> fetchAll();
        $contador = 0;

        foreach($datos as $daticos){
            $daticos['envia']=$_SESSION['cifrador']->descifrar($daticos['envia']);
            $daticos['recibe']=$_SESSION['cifrador']->descifrar($daticos['recibe']);

            if (($daticos['recibe'])==$_SESSION['usuario']->nick) {
                if($daticos['leido']==0){
                    $contador++;
                }
            }
        }

        return  $contador;
    }

}