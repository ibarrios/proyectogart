<?php


class Item {


    function crearItem(){
        $pdo=new PDO("mysql:host=localhost;dbname=proyectogar;charset=utf8", 'root', '');

        $titulo = filter_var($_POST["titulo"], FILTER_SANITIZE_STRING);
        $juego = $_SESSION['cifrador']  -> cifrar(filter_var($_POST["juego"], FILTER_SANITIZE_STRING));
        $compania = $_SESSION['cifrador']  ->cifrar(filter_var($_POST["compania"], FILTER_SANITIZE_STRING));
        $fecha = $_SESSION['cifrador']  ->cifrar(filter_var($_POST["fecha"], FILTER_SANITIZE_EMAIL));
        $categoria = $_POST["categoria"];
        if (isset($_SESSION['usuario']->nick)){
            $autor_entrada= $_SESSION['cifrador']->cifrar($_SESSION['usuario']->nick);
        } else {
            $autor_entrada= $_SESSION['cifrador']->cifrar("admin");
        }
        if ($categoria =="youtube"){

           $enlace = substr(($_POST['enlace']),-11);

        } else {
            $enlace = $_POST['enlace'];
        }

        $sql = "INSERT INTO item (titulo, juego, compania, fecha,autor_entrada,categoria,enlace) VALUES (
            :titulo, 
            :juego, 
            :compania,                                                             
            :fecha, 
            :autor_entrada,
            :categoria,
            :enlace)";
        $stmt =$pdo ->prepare($sql);

        $stmt->bindParam(':titulo', $titulo, PDO::PARAM_STR);
        $stmt->bindParam(':juego', $juego, PDO::PARAM_STR);
        $stmt->bindParam(':compania', $compania, PDO::PARAM_STR);
        $stmt->bindParam(':fecha', $fecha, PDO::PARAM_STR);
        $stmt->bindParam(':autor_entrada', $autor_entrada, PDO::PARAM_STR);
        $stmt->bindParam(':categoria', $categoria, PDO::PARAM_STR);
        $stmt->bindParam(':enlace', $enlace, PDO::PARAM_STR);

        $stmt->execute();
        $_SESSION['mensaje'] = 'Item Creado';
        header('Location:' . getenv('HTTP_REFERER'));
    }



    function buscarTitulo($tabla,$titulo,$user,$mostrar){
        $pdo=new PDO("mysql:host=localhost;dbname=proyectogar;charset=utf8", 'root', '');

        if ($mostrar == 1){

            $consulta = $pdo ->prepare("select * from item order by 1 desc");
        } else if ($mostrar == 2){
            $consulta = $pdo->query ("SELECT * FROM `item` inner join likes on likes.id_item = item.id inner join usuario 
                on likes.id_usuario = usuario.id where usuario.nick = '".$_SESSION['cifrador']->cifrar($user)."' order by 1 desc");

        } else {
            if ($tabla == "titulo"){

                $consulta = $pdo ->prepare("select * from item where titulo like '%".$titulo."%' order by 1 desc");
            } else {
                if ($tabla != "categoria") {
                    $titulo = $_SESSION['cifrador']->cifrar($titulo);
                }
                $consulta = $pdo ->prepare("select * from item where ".$tabla." = '".$titulo."' order by 1 desc");

            }
        }
        $consulta->execute();
        $datos = $consulta -> fetchAll();

        foreach($datos as $daticos) {

            $daticos['juego'] = $_SESSION['cifrador']->descifrar($daticos['juego']);
            $daticos['fecha'] = $_SESSION['cifrador']->descifrar($daticos['fecha']);
            $daticos['compania'] = $_SESSION['cifrador']->descifrar($daticos['compania']);
            $daticos['autor_entrada'] = $_SESSION['cifrador']->descifrar($daticos['autor_entrada']);
            $enlace = $daticos['enlace'];

            if ($daticos['categoria'] == "video") {
                echo "<article>
                    <h2>" . $daticos['titulo'] . "</h2>
            <br>
                    <div class='embed-responsive embed-responsive-16by9'>
                        <iframe class='embed-responsive-item' src='" . $daticos['enlace'] . "' allowfullscreen></iframe>
                    </div><br>
                    <ul>";

            } else if ($daticos['categoria'] == "Audio") {
                echo " <article>
                     <h2> " . $daticos['titulo'] . " </h2>
                <br>
                    <div class='col-sm-4 col-sm-offset-4 embed-responsive embed-responsive-4by3'>
                        <audio controls class='embed-responsive-item'>
                            <source src='" . $daticos['enlace'] . "'>
                        </audio>
                    </div><br>
                     <ul>";

            } else if ($daticos['categoria'] == "Imagen") {

                echo " <article>
                     <h2>" . $daticos['titulo'] . "</h2>
            <br>
                    <img class='img-fluid' alt='Responsive image' src='" . $daticos['enlace'] . "'>
                    <br><br>
                    <ul>";


            } else if ($daticos['categoria'] == "youtube") {
                echo " <article>
                         <h2>" . $daticos['titulo'] . "</h2>
<br>
                <div class='embed-responsive embed-responsive-16by9'>";
                echo "<iframe width=\"250\" height=\"140\" src=\"http://www.youtube.com/embed/" . $enlace . "?rel=0\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>";

                echo "</div><br>

      <ul>";
            }

            echo" 
                            <form class='sinborde' action='buscador.php'  method='post'>
                           
                            <li>Juego: <input type ='text' name='juego' value='" . $daticos['juego'] . "' onclick='this.form.submit()' readonly class ='sinborde'>
                            
                            </li></form>";

            echo" 
                            <form class='sinborde' action='buscador.php'  method='post'>
                           
                            <li>Compañia: <input type ='text' name='compania' value='" . $daticos['compania'] . "' onclick='this.form.submit()' readonly class ='sinborde'>
                            
                            </li></form>";


            echo" 
                            <form class='sinborde' action='buscador.php'  method='post'>
                           
                            <li>Año: <input type ='text' name='ano' value='" . $daticos['fecha'] . "' onclick='this.form.submit()' readonly class ='sinborde'>
                            
                            </li></form>";


            echo" 
                            <form class='sinborde' action='buscador.php'  method='post'>
                           
                            <li>Categoría: <input type ='text' name='categoria' value='" . $daticos['categoria'] . "' onclick='this.form.submit()' readonly class ='sinborde'>
                            
                            </li></form>";


            echo" 
                            <form class='sinborde' action='buscador.php'  method='post'>
                           
                            <li>Autor: <input type ='text' name='autor' value='" . $daticos['autor_entrada'] . "' onclick='this.form.submit()' readonly class ='sinborde'>
                            
                            </li></form>";
            if($mostrar == 1 || $mostrar == 0){
                if (isset($_SESSION['usuario']->nick)){
                    echo "<form action='acciones.php' method='post'>";
                    if (!$_SESSION['item']->existeFav($daticos['id'],$_SESSION['usuario']->id)) {
                        echo "Favorito<input type='checkbox'  name='itemFavorito' onchange='this.form.submit()' value='fav'>";
                    } else {
                        echo "Favorito<input type='checkbox'  name='quitarFav' onchange='this.form.submit()' value='fav' checked>";
                    }
                    echo "<input type='hidden' name ='idDelItem' value='".$daticos['id']."'>";
                    echo "</form>";
                }
            } else if ($mostrar == 2) {
                echo "<form action='acciones.php' method='post'>";
                echo "Favorito<input type='checkbox'  name='quitarFav' onchange='this.form.submit()' value='fav' checked>";
                echo "<input type='hidden' name ='idDelItem' value='".$daticos['id_item']."'>";
                echo "</form>";
            }
            echo "</ul>
            </article>";

        }
    }




    function existeFav($idPiso,$idUser) {

        $pdo=new PDO("mysql:host=localhost;dbname=proyectogar;charset=utf8", 'root', '');
        $consulta = $pdo ->prepare("select * from likes where id_item = '".$idPiso."' and id_usuario = '".$idUser."' ");
        $consulta->execute();
        $existeFav = $consulta -> rowCount();


        if ($existeFav == 1) {
            return true ;
        } else {
            return false;
        }
    }

    function listadoItems(){
        $pdo=new PDO("mysql:host=localhost;dbname=proyectogar;charset=utf8", 'root', '');
        $consulta = $pdo->query ("SELECT * FROM `item`");
        $consulta->execute();
        $datos = $consulta -> fetchAll();
        $cadena = "'subir_imagen.php'";
        foreach ($datos as $daticos) {

            $daticos['juego']=$_SESSION['cifrador']->descifrar($daticos['juego']);
            $daticos['fecha']=$_SESSION['cifrador']->descifrar($daticos['fecha']);
            $daticos['compania']=$_SESSION['cifrador']->descifrar($daticos['compania']);


            echo "<form action='acciones.php' method='post'>";
            echo "<input type='text'  name = 'elPiso' value='".$daticos['titulo']."'></br>";
            echo "<input type='text'  name = 'elJuego' value='".$daticos['juego']."'></br>";
            echo "<input type='text'  name = 'laFecha' value='".$daticos['fecha']."'></br>";
            echo "<input type='text'  name = 'laCompania' value='".$daticos['compania']."'></br>";
            echo "<select name='categoria' class='form-select' aria-label='Default select example'>
                    <option selected='".$daticos['categoria']."'>".$daticos['categoria']."</option>
                   <option value='video'>Video</option>
                    <option value='Audio'>Audio</option>
                    <option value='youtube'>Youtube</option>
                    <option value='Imagen'>Imagen</option>
                     </select></br>";

            echo "<input type='hidden' name='elId' value='".$daticos['id']."'>";
            echo "<input type='submit' class='btn btn-primary' name='borrarItem' value='Borrar'>";
            echo "<input type='submit' class='btn btn-primary' name='editarItem' value='editar'>";
            echo "<input type='hidden' value='".$daticos['enlace']."' name='elEnlace' id=''/>";

            echo "</form>";
            echo "<input type='text' value='".$daticos['enlace']."' name='elEnlace' id=''/> Contenido actual
</br></br>";

            if (!$daticos['categoria'] == "youtube"){

                echo "<form action='acciones.php' enctype='multipart/form-data' method='post'>
                <input type='hidden' name='elId' value='".$daticos['id']."'>
                <input type='hidden'  name = 'laCategoria' value='".$daticos['categoria']."'>
                                        <input id='imagen'  name='imagen' size='30' type='file' />
                                        <input type='submit' class='btn btn-primary'name='elContenido' value='Modificar contenido' />
                                    </form>";
            } else {
                echo "<form action='acciones.php' enctype='multipart/form-data' method='post'>
                <input type='hidden' name='elId' value='".$daticos['id']."'>
                <input type='hidden'  name = 'laCategoria' value='".$daticos['categoria']."'>
                <input id='imagen'  name='imagen'  type='text' />
               <input type='submit' class='btn btn-primary'name='elContenido' value='Modificar contenido' />
                                    </form>";
            }

        }

    }


    function modificarContenido($elId, $elContenido){
        $pdo=new PDO("mysql:host=localhost;dbname=proyectogar;charset=utf8", 'root', '');
        $sql = "UPDATE item SET  enlace='".$elContenido."' where id = '".$elId. "' ";
        $pdo->prepare($sql)->execute();

        header('Location:crearitem.php');

    }

    function editarItem($titulo,$juego,$compania,$fecha,$categoria,$enlace,$id){
        $pdo=new PDO("mysql:host=localhost;dbname=proyectogar;charset=utf8", 'root', '');
        $juego = $_SESSION['cifrador']  -> cifrar(filter_var($juego, FILTER_SANITIZE_STRING));
        $compania = $_SESSION['cifrador']  ->cifrar(filter_var($compania, FILTER_SANITIZE_STRING));
        $fecha = $_SESSION['cifrador']  ->cifrar(filter_var($fecha, FILTER_SANITIZE_EMAIL));

        $sql = "UPDATE item SET titulo='".$titulo."', juego='".$juego."', compania='".$compania."', fecha='".$fecha."', categoria='".$categoria."', enlace='".$enlace."' where id = '".$id. "' ";

        $pdo->prepare($sql)->execute();

        header('Location:crearitem.php');
    }

    function borrarItem($id){
        $pdo=new PDO("mysql:host=localhost;dbname=proyectogar;charset=utf8", 'root', '');
        $consulta = $pdo->query ("delete from likes where id_item = ".$id." ");
        $consulta->execute();
        $consulta = $pdo->query ("delete from item where id = ".$id." ");
        $consulta->execute();

        header('Location:' . getenv('HTTP_REFERER'));
    }



}