-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 28, 2021 at 11:22 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `proyectogar`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `nick` varchar(45) NOT NULL,
  `pass` varchar(45) NOT NULL,
  `userCrea` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `nick`, `pass`, `userCrea`) VALUES
(1, 'admin', 'admin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `buzon`
--

CREATE TABLE `buzon` (
  `id` int(11) NOT NULL,
  `envia` varchar(200) NOT NULL,
  `recibe` varchar(200) NOT NULL,
  `leido` tinyint(1) NOT NULL,
  `contenido` varchar(400) NOT NULL,
  `id_usuario_envia` int(11) NOT NULL,
  `id_usuario_recibe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `buzon`
--

INSERT INTO `buzon` (`id`, `envia`, `recibe`, `leido`, `contenido`, `id_usuario_envia`, `id_usuario_recibe`) VALUES
(4, 'LNbBEGX/jIL5uclqP9rJ5Q==', 'CF9rku+tFVKVehXTrR+Dpw==', 1, 'IMGoPFShZFoKRK9acs/22Q==', 23, 22),
(6, 'yd0DgapzMZsgy9afClytHg==', 'LNbBEGX/jIL5uclqP9rJ5Q==', 1, 's4DVwkqjXH/isDuPhcIMhA==', 24, 23),
(9, 'LNbBEGX/jIL5uclqP9rJ5Q==', 'yd0DgapzMZsgy9afClytHg==', 0, 'hHZiDaBURBn53a2JDNlr+w==', 23, 24),
(10, 'LNbBEGX/jIL5uclqP9rJ5Q==', 'CF9rku+tFVKVehXTrR+Dpw==', 0, 'YQr27cERqJB5jdHYKyLY6w==', 23, 22),
(11, 'LNbBEGX/jIL5uclqP9rJ5Q==', 'CF9rku+tFVKVehXTrR+Dpw==', 0, 'NYNhf5KccVoTqn7IEMuI4w==', 23, 22),
(12, 'LNbBEGX/jIL5uclqP9rJ5Q==', 'CF9rku+tFVKVehXTrR+Dpw==', 0, 'XanIGspS46ErvwKOm7R8TA==', 23, 22),
(13, 'LNbBEGX/jIL5uclqP9rJ5Q==', 'yd0DgapzMZsgy9afClytHg==', 0, 'XanIGspS46ErvwKOm7R8TA==', 23, 24);

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `id` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `juego` varchar(200) NOT NULL,
  `compania` varchar(200) NOT NULL,
  `fecha` varchar(200) NOT NULL,
  `autor_entrada` varchar(200) NOT NULL,
  `categoria` varchar(45) NOT NULL,
  `enlace` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`id`, `titulo`, `juego`, `compania`, `fecha`, `autor_entrada`, `categoria`, `enlace`) VALUES
(22, 'Dark Souls Primer Trailer', 'nlJ2sTBos1KPOgRA778Zqg==', 'wXS6YnKBvIELHLrBglva8g==', 'rZqAgfwX8vxEG4pWfwoIYw==', '2ABLLsUgNii6xobPKw6siA==', 'video', 'img/29851971.mp4'),
(24, 'Dark Souls 3 Boxart', 'uGVRUfIG3yIvVwTXoabCPg==', 'wXS6YnKBvIELHLrBglva8g==', 'CqYJ2dU23hVqbH3mO9fbug==', 'LNbBEGX/jIL5uclqP9rJ5Q==', 'Imagen', 'img/09295208.jpg'),
(26, 'Zelda Breath of the Wild Main Theme', 'tn+hKPrzI+OWy5hDqIUJJ5UEK+nmyhyIYEvCxq4CRQ9pXX9pFW+bsd7z1JVp8Zrl', 'I1thFwF0vL2xSCHpyWMlOQ==', 'fq2W3Q+bYEBVR8BzqJeMJA==', 'CF9rku+tFVKVehXTrR+Dpw==', 'Audio', 'img/336105902.mp3'),
(27, 'Zelda Breath of the Wild Teaser', 'tn+hKPrzI+OWy5hDqIUJJ5UEK+nmyhyIYEvCxq4CRQ9pXX9pFW+bsd7z1JVp8Zrl', 'I1thFwF0vL2xSCHpyWMlOQ==', 'fq2W3Q+bYEBVR8BzqJeMJA==', 'LNbBEGX/jIL5uclqP9rJ5Q==', 'youtube', 'jmrzIIIbB10'),
(28, 'Persona 5 trailer de lanzamiento', 'FmLzcUvMRJRIlrPV6qBF3Q==', 'jzBCRQoiPs+iNVkbNA1skQ==', 'fq2W3Q+bYEBVR8BzqJeMJA==', 'LNbBEGX/jIL5uclqP9rJ5Q==', 'youtube', 'QnDzJ9KzuV4'),
(29, 'Persona 3 Boxart', 'EyfdDpjX2u783oKV+ikr/g==', 'jzBCRQoiPs+iNVkbNA1skQ==', 'sjT6PsZ7uKYTVyl5zgxHTA==', '2ABLLsUgNii6xobPKw6siA==', 'Imagen', 'img/366375513.jpg'),
(30, 'Super Mario Galaxy artwork', 'thcQbNHbAf5XrEJnPQj2AhekQq3YU34E9RRnxfWAkBc=', 'I1thFwF0vL2xSCHpyWMlOQ==', 'ITYpudMOVoveotax7YLabQ==', '2ABLLsUgNii6xobPKw6siA==', 'Imagen', 'img/449609648.jpg'),
(31, 'Gwyn Lord of Cinder', 'nlJ2sTBos1KPOgRA778Zqg==', 'wXS6YnKBvIELHLrBglva8g==', '2g0A1OSyAlaAso2ovFFRsw==', '2ABLLsUgNii6xobPKw6siA==', 'Imagen', 'img/338948334.jpg'),
(32, 'Metroid theme', 'kZXNxyb3IF+j9jEUwgPU2A==', 'I1thFwF0vL2xSCHpyWMlOQ==', 'Gv8q7CXWTjsn5Mv6yZEz5g==', 'CF9rku+tFVKVehXTrR+Dpw==', 'Audio', 'img/27413550.mp3'),
(33, 'The Last of Us launch trailer', 'NxjrryKtdmyP5rF31Q29lw==', 'DpKGvyR5Mktp0okR1i81ZA==', 'kwJeFf/AhuWB2gzlcTGHqw==', '2ABLLsUgNii6xobPKw6siA==', 'video', 'img/795713254.mp4'),
(34, 'The Last of Us Part 2 launch trailer', 'oqQlS0MRQGoTPLhN0/B96+cqpSCH49k96kO0lloWxPo=', 'DpKGvyR5Mktp0okR1i81ZA==', '8X/rDyBBkJblA+F+duE9Uw==', 'LNbBEGX/jIL5uclqP9rJ5Q==', 'video', 'img/847596766.mp4');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `id_item`, `id_usuario`) VALUES
(23, 29, 23),
(26, 22, 23);

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `apellidos` varchar(200) NOT NULL,
  `telefono` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `nick` varchar(200) NOT NULL,
  `pass` varchar(200) NOT NULL,
  `avatar` varchar(200) NOT NULL,
  `id_admin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`id`, `nombre`, `apellidos`, `telefono`, `email`, `nick`, `pass`, `avatar`, `id_admin`) VALUES
(22, 'CF9rku+tFVKVehXTrR+Dpw==', 'hICP9nJ8Cd5tcKyChQ4xzw==', 'J71DisZ2lqiY+JdsV/C0zw==', 'K0sg1qOlE5Y1mjIQg+lIww==', 'CF9rku+tFVKVehXTrR+Dpw==', '$2y$10$FMNx9bneSL2Blycog8yM3.vuXMkQAlkT4PVpHzV8Bcp5DQXsqoh0W', '', 1),
(23, '2RuVN/vj6aghvs1QO/71AQ==', '', '', '26WojE4FMs/ZQRYiSHHP8trvRCkDF9LhWnODi5HsY+I=', 'LNbBEGX/jIL5uclqP9rJ5Q==', '$2y$10$.nEdahtpOhIe88L8XG4L0uEQj1myDb0el2UAoxVE3yNFfhh9hMKOG', 'img/1024-1-768x511.jpg', 1),
(24, 'k1MzRJjHtD0VwvIx3Ri1YA==', 'sUbiiAgvGz8GBWo6NDeLKA==', 'J71DisZ2lqiY+JdsV/C0zw==', '26WojE4FMs/ZQRYiSHHP8trvRCkDF9LhWnODi5HsY+I=', 'yd0DgapzMZsgy9afClytHg==', '$2y$10$1LPsKYWKHsC1AJlsVQcPVuYNvzJX3ioQIY.mAXyNnI75xz0OKF02K', '', 1),
(25, 'RRH4FUwKIoHjvLSXH2FHew==', 'j0gc4auVrBH1E++2voeSEw==', 'HQZzqMPeJYKh3MoBK09zaw==', '6zNeHc5qorE33uwFLdbdsw==', 'k1MzRJjHtD0VwvIx3Ri1YA==', '$2y$10$W8uZ/YWaBO0TT7HKRPeUxO88J5pMzpt98IMj09czKYFFRY4W7NAq.', '', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `buzon`
--
ALTER TABLE `buzon`
  ADD PRIMARY KEY (`id`,`id_usuario_envia`,`id_usuario_recibe`),
  ADD KEY `id` (`id`),
  ADD KEY `id_usuario_envia` (`id_usuario_envia`,`id_usuario_recibe`),
  ADD KEY `id_usuario_recibe` (`id_usuario_recibe`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`,`id_item`,`id_usuario`),
  ADD KEY `id_item` (`id_item`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_admin` (`id_admin`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `buzon`
--
ALTER TABLE `buzon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `buzon`
--
ALTER TABLE `buzon`
  ADD CONSTRAINT `buzon_ibfk_1` FOREIGN KEY (`id_usuario_envia`) REFERENCES `usuario` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `buzon_ibfk_2` FOREIGN KEY (`id_usuario_recibe`) REFERENCES `usuario` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `likes`
--
ALTER TABLE `likes`
  ADD CONSTRAINT `likes_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `likes_ibfk_2` FOREIGN KEY (`id_item`) REFERENCES `item` (`id`);

--
-- Constraints for table `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`id_admin`) REFERENCES `admin` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
